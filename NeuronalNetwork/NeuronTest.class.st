"
Neuron tests including:
 - basic logical constructs
 - training over some epochs and then test what was learned
"
Class {
	#name : #NeuronTest,
	#superclass : #TestCase,
	#category : #'NeuronalNetwork-Tests'
}

{ #category : #accessing }
NeuronTest >> digitalComparator: inputs [
    "Return an array of three elements"
    | not and nor a b aGb aEb aLb notA notB |
    a := inputs first.
    b := inputs second.

    and := Neuron new weights: #(1 1); bias: -1.5.
    not := Neuron new weights: #(-1); bias: 0.5.
    nor := Neuron new weights: #(-1 -1); bias: 0.5. 

    notA := not feed: { a }. 
    notB := not feed: { b }.
    
    aLb := and feed: { notA . b }.
    aGb := and feed: { a . notB }.
    aEb := nor feed: { aGb . aLb }.
    ^ { aGb . aEb . aLb }
]

{ #category : #tests }
NeuronTest >> testAND [
    | p |
    p := Neuron new.
    p weights: #(1 1).
    p bias: -1.5.
    
    self assert: (p feed: #(0 0)) equals: 0.
    self assert: (p feed: #(0 1)) equals: 0.
    self assert: (p feed: #(1 0)) equals: 0.
    self assert: (p feed: #(1 1)) equals: 1.
]

{ #category : #tests }
NeuronTest >> testDigitalComparator [
    self assert: (self digitalComparator: #(0 0)) equals: #(0 1 0).
    self assert: (self digitalComparator: #(0 1)) equals: #(0 0 1).
    self assert: (self digitalComparator: #(1 0)) equals: #(1 0 0).
    self assert: (self digitalComparator: #(1 1)) equals: #(0 1 0).
]

{ #category : #tests }
NeuronTest >> testNOR [
    | p |
    p := Neuron new.
    p weights: #(-1 -1).
    p bias: 0.5.
    
    self assert: (p feed: #(0 0)) equals: 1.
    self assert: (p feed: #(0 1)) equals: 0.
    self assert: (p feed: #(1 0)) equals: 0.
    self assert: (p feed: #(1 1)) equals: 0.
]

{ #category : #tests }
NeuronTest >> testNOT [
    | p |
    p := Neuron new.
    p weights: #(-1).
    p bias: 0.5.
    
    self assert: (p feed: #(1)) equals: 0.
    self assert: (p feed: #(0)) equals: 1.
]

{ #category : #tests }
NeuronTest >> testOR [
    | p |
    p := Neuron new.
    p weights: #(1 1).
    p bias: -0.5.
    
    self assert: (p feed: #(0 0)) equals: 0.
    self assert: (p feed: #(0 1)) equals: 1.
    self assert: (p feed: #(1 0)) equals: 1.
    self assert: (p feed: #(1 1)) equals: 1.
]

{ #category : #tests }
NeuronTest >> testSmallExample [
    | p result |
    p := Neuron new.
    p weights: #(1 2).
    p bias: -2.
    result := p feed: #(5 2).
    self assert: result equals: 1.
]

{ #category : #tests }
NeuronTest >> testSmallExample2 [
    | p result |
    p := Neuron new.
    p weights: #(-2 2).
    p bias: -2.
    result := p feed: #(5 2).
    self assert: result equals: 0.
]

{ #category : #tests }
NeuronTest >> testTrainingAND [
	| p |
    p := Neuron new.
    p weights: #(1 1).
    p bias: 1.
    
    12 timesRepeat: [ 
        p train: #(0 0) desiredOutput: 1.
        p train: #(0 1) desiredOutput: 0.
        p train: #(1 0) desiredOutput: 0.
        p train: #(1 1) desiredOutput: 1.
    ].
    
    self assert: (p feed: #(0 0)) equals: 1.
    self assert: (p feed: #(0 1)) equals: 0.
    self assert: (p feed: #(1 0)) equals: 0.
    self assert: (p feed: #(1 1)) equals: 1.
]

{ #category : #tests }
NeuronTest >> testTrainingHighLearningRateAND [
	| p |
	
	p := Neuron new.
	p learningRate: 0.5.
	p weights: #(1 1).
   p bias: 1.
    
   5 timesRepeat: [ 
		p train: #(0 0) desiredOutput: 1.
      p train: #(0 1) desiredOutput: 0.
      p train: #(1 0) desiredOutput: 0.
      p train: #(1 1) desiredOutput: 1.
	].
    
 	self assert: (p feed: #(0 0)) equals: 1.
   self assert: (p feed: #(0 1)) equals: 0.
   self assert: (p feed: #(1 0)) equals: 0.
   self assert: (p feed: #(1 1)) equals: 1.
]

{ #category : #tests }
NeuronTest >> testTrainingNOR [
 | p |
    p := Neuron new.
    p weights: #(-1 -1).
    p bias: 2.
    
    4 timesRepeat: [ 
        p train: #(0 0) desiredOutput: 1.
        p train: #(0 1) desiredOutput: 0.
        p train: #(1 0) desiredOutput: 0.
        p train: #(1 1) desiredOutput: 0.
    ].
    
    self assert: (p feed: #(0 0)) equals: 1.
    self assert: (p feed: #(0 1)) equals: 0.
    self assert: (p feed: #(1 0)) equals: 0.
    self assert: (p feed: #(1 1)) equals: 0.
]

{ #category : #tests }
NeuronTest >> testTrainingNOT [
    | p |
    p := Neuron new.
    p weights: #(-1).
    p bias: 2.
    
    5 timesRepeat: [ 
        p train: #(0) desiredOutput: 1.
        p train: #(1) desiredOutput: 0.
    ].
    
    self assert: (p feed: #(0)) equals: 1.
    self assert: (p feed: #(1)) equals: 0.
]

{ #category : #tests }
NeuronTest >> testTrainingOR [
    | p |
    p := Neuron new.
    p weights: #(-1 -1).
    p bias: 2.
    
    32 timesRepeat: [ 
        p train: #(0 0) desiredOutput: 0.
        p train: #(0 1) desiredOutput: 1.
        p train: #(1 0) desiredOutput: 1.
        p train: #(1 1) desiredOutput: 1.
    ].
    
    self assert: (p feed: #(0 0)) equals: 0.
    self assert: (p feed: #(0 1)) equals: 1.
    self assert: (p feed: #(1 0)) equals: 1.
    self assert: (p feed: #(1 1)) equals: 1.
]

{ #category : #tests }
NeuronTest >> testWrongFeeding [
    | p |
    p := Neuron new.
    p weights: #(-1).
    p bias: 0.5.
    
    self should: [ p feed: #(1 1) ] raise: Error
]
