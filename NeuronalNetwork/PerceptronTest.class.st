"
Perceptron tests including:
 - basic logical constructs
 - training over some epochs and then test what was learned
"
Class {
	#name : #PerceptronTest,
	#superclass : #TestCase,
	#category : #'NeuronalNetwork-Tests'
}

{ #category : #accessing }
PerceptronTest >> digitalComparator: inputs [
    "Return an array of three elements"
    | not and nor a b aGb aEb aLb notA notB |
    a := inputs first.
    b := inputs second.

    and := Perceptron new weights: #(1 1); bias: -1.5.
    not := Perceptron new weights: #(-1); bias: 0.5.
    nor := Perceptron new weights: #(-1 -1); bias: 0.5. 

    notA := not feed: { a }. 
    notB := not feed: { b }.
    
    aLb := and feed: { notA . b }.
    aGb := and feed: { a . notB }.
    aEb := nor feed: { aGb . aLb }.
    ^ { aGb . aEb . aLb }
]

{ #category : #tests }
PerceptronTest >> testAND [
    | p |
    p := Perceptron new.
    p weights: #(1 1).
    p bias: -1.5.
    
    self assert: (p feed: #(0 0)) equals: 0.
    self assert: (p feed: #(0 1)) equals: 0.
    self assert: (p feed: #(1 0)) equals: 0.
    self assert: (p feed: #(1 1)) equals: 1.
]

{ #category : #tests }
PerceptronTest >> testDigitalComparator [
    self assert: (self digitalComparator: #(0 0)) equals: #(0 1 0).
    self assert: (self digitalComparator: #(0 1)) equals: #(0 0 1).
    self assert: (self digitalComparator: #(1 0)) equals: #(1 0 0).
    self assert: (self digitalComparator: #(1 1)) equals: #(0 1 0).
]

{ #category : #tests }
PerceptronTest >> testNOR [
    | p |
    p := Perceptron new.
    p weights: #(-1 -1).
    p bias: 0.5.
    
    self assert: (p feed: #(0 0)) equals: 1.
    self assert: (p feed: #(0 1)) equals: 0.
    self assert: (p feed: #(1 0)) equals: 0.
    self assert: (p feed: #(1 1)) equals: 0.
]

{ #category : #tests }
PerceptronTest >> testNOT [
    | p |
    p := Perceptron new.
    p weights: #(-1).
    p bias: 0.5.
    
    self assert: (p feed: #(1)) equals: 0.
    self assert: (p feed: #(0)) equals: 1.
]

{ #category : #tests }
PerceptronTest >> testOR [
    | p |
    p := Perceptron new.
    p weights: #(1 1).
    p bias: -0.5.
    
    self assert: (p feed: #(0 0)) equals: 0.
    self assert: (p feed: #(0 1)) equals: 1.
    self assert: (p feed: #(1 0)) equals: 1.
    self assert: (p feed: #(1 1)) equals: 1.
]

{ #category : #tests }
PerceptronTest >> testSmallExample [
    | p result |
    p := Perceptron new.
    p weights: #(1 2).
    p bias: -2.
    result := p feed: #(5 2).
    self assert: result equals: 1.
]

{ #category : #tests }
PerceptronTest >> testSmallExample2 [
    | p result |
    p := Perceptron new.
    p weights: #(-2 2).
    p bias: -2.
    result := p feed: #(5 2).
    self assert: result equals: 0.
]

{ #category : #tests }
PerceptronTest >> testTrainingAND [
	| p |
    p := Perceptron new.
    p weights: #(1 1).
    p bias: 1.
    
    12 timesRepeat: [ 
        p train: #(0 0) desiredOutput: 1.
        p train: #(0 1) desiredOutput: 0.
        p train: #(1 0) desiredOutput: 0.
        p train: #(1 1) desiredOutput: 1.
    ].
    
    self assert: (p feed: #(0 0)) equals: 1.
    self assert: (p feed: #(0 1)) equals: 0.
    self assert: (p feed: #(1 0)) equals: 0.
    self assert: (p feed: #(1 1)) equals: 1.
]

{ #category : #tests }
PerceptronTest >> testTrainingHighLearningRateAND [
	| p |
	
	p := Perceptron new.
	p learningRate: 0.5.
	p weights: #(1 1).
   p bias: 1.
    
   5 timesRepeat: [ 
		p train: #(0 0) desiredOutput: 1.
      p train: #(0 1) desiredOutput: 0.
      p train: #(1 0) desiredOutput: 0.
      p train: #(1 1) desiredOutput: 1.
	].
    
 	self assert: (p feed: #(0 0)) equals: 1.
   self assert: (p feed: #(0 1)) equals: 0.
   self assert: (p feed: #(1 0)) equals: 0.
   self assert: (p feed: #(1 1)) equals: 1.
]

{ #category : #tests }
PerceptronTest >> testTrainingNOR [
 | p |
    p := Perceptron new.
    p weights: #(-1 -1).
    p bias: 2.
    
    4 timesRepeat: [ 
        p train: #(0 0) desiredOutput: 1.
        p train: #(0 1) desiredOutput: 0.
        p train: #(1 0) desiredOutput: 0.
        p train: #(1 1) desiredOutput: 0.
    ].
    
    self assert: (p feed: #(0 0)) equals: 1.
    self assert: (p feed: #(0 1)) equals: 0.
    self assert: (p feed: #(1 0)) equals: 0.
    self assert: (p feed: #(1 1)) equals: 0.
]

{ #category : #tests }
PerceptronTest >> testTrainingNOT [
    | p |
    p := Perceptron new.
    p weights: #(-1).
    p bias: 2.
    
    5 timesRepeat: [ 
        p train: #(0) desiredOutput: 1.
        p train: #(1) desiredOutput: 0.
    ].
    
    self assert: (p feed: #(0)) equals: 1.
    self assert: (p feed: #(1)) equals: 0.
]

{ #category : #tests }
PerceptronTest >> testTrainingOR [
    | p |
    p := Perceptron new.
    p weights: #(-1 -1).
    p bias: 2.
    
    32 timesRepeat: [ 
        p train: #(0 0) desiredOutput: 0.
        p train: #(0 1) desiredOutput: 1.
        p train: #(1 0) desiredOutput: 1.
        p train: #(1 1) desiredOutput: 1.
    ].
    
    self assert: (p feed: #(0 0)) equals: 0.
    self assert: (p feed: #(0 1)) equals: 1.
    self assert: (p feed: #(1 0)) equals: 1.
    self assert: (p feed: #(1 1)) equals: 1.
]

{ #category : #tests }
PerceptronTest >> testWrongFeeding [
    | p |
    p := Perceptron new.
    p weights: #(-1).
    p bias: 0.5.
    
    self should: [ p feed: #(1 1) ] raise: Error
]
