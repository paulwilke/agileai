"
Perceptron as described in Agile AI

https://agileartificialintelligence.github.io/book/build/02-Perceptron.html

""Setting up a Perceptron with initial parameters""
p := Perceptron new
		weights: #(-1 -1);
		bias: 2;
		yourself.

""Running 10 epochs to train the Perceptron and let it learn""
10 timesRepeat: [ p train: #(0 1) desiredOutput: 0 ].

""Feed the Perceptron with real data and let it answer""
p feed: #(0 1).
"
Class {
	#name : #Perceptron,
	#superclass : #Object,
	#instVars : [
		'weights',
		'bias',
		'learningRate'
	],
	#category : #'NeuronalNetwork-Base'
}

{ #category : #accessing }
Perceptron >> bias [
    ^ bias
]

{ #category : #accessing }
Perceptron >> bias: aNumber [
    bias := aNumber
]

{ #category : #accessing }
Perceptron >> feed: inputs [
    | z |
    z := (inputs with: weights collect: [ :x :w | x * w ]) sum + bias.
    ^ z > 0 ifTrue: [ 1 ] ifFalse: [ 0 ].
]

{ #category : #initialization }
Perceptron >> initialize [
    super initialize.
    learningRate := 0.1
]

{ #category : #accessing }
Perceptron >> learningRate [
    "Return the learning rate of the neuron"
    ^ learningRate
]

{ #category : #accessing }
Perceptron >> learningRate: aNumber [
    "Set the learning rate of the neuron"
    learningRate := aNumber
]

{ #category : #accessing }
Perceptron >> train: inputs desiredOutput: desiredOutput [
    | theError output newWeight |
    output := self feed: inputs.
    theError := desiredOutput - output.
    inputs
        withIndexDo: [ :anInput :index | 
            newWeight := (weights at: index) + (learningRate * theError * anInput).
            weights at: index put: newWeight ].
    bias := bias + (learningRate * theError)
]

{ #category : #accessing }
Perceptron >> weights [
    "Return the weights of the neuron."
    ^ weights
]

{ #category : #accessing }
Perceptron >> weights: someWeightsAsNumbers [
    weights := someWeightsAsNumbers
]
